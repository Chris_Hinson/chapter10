//Chris Hinson Chapter 10
//program 2 subclass

public class ShiftSupervisor extends Employee
{
    protected int salary;
    protected int bonus;

    public ShiftSupervisor(String name, String id, String hire_date, int salary, int bonus) {
        super(name, id, hire_date);
        this.salary = salary;
        this.bonus = bonus;
    }

    public int getSalary() {
        return salary;
    }
    public int getBonus() {
        return bonus;
    }



    public void setSalary(int salary) {
        this.salary = salary;
    }
    public void setBonus(int bonus) {
        this.bonus = bonus;
    }
}
