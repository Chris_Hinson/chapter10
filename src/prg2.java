//Chris Hinson Chapter 10
//program 2 demonstration class

import java.util.Scanner;

public class prg2
{

    public static void main(String args[]) {
        Scanner k = new Scanner(System.in);
        System.out.println("Employee name?");
        String name = k.next();
        System.out.println("Employee id? - please use correct numbernumbernumber-letter formatting");
        String id = k.next();
        System.out.println("Hire date?(mm/dd/yyyy)");
        String hire_date = k.next();
        System.out.println("Salary?");
        int salary = k.nextInt();
        System.out.println("Bonus so far this year?");
        int bonus = k.nextInt();

        ShiftSupervisor chris = new ShiftSupervisor(name, id, hire_date, salary, bonus);
    }
}
