//Chris Hinson Chapter 10
//program 3 subclass

public class TeamLeader extends ProductionWorker
{
    protected int monthly_bonus;
    protected int traing_hours_required = 50;
    protected int training_hours_completed;

    public TeamLeader(String name, String id, String hire_date, int shift, double payrate, int monthly_bonus, int training_hours_completed)
    {
        super(name, id, hire_date, shift, payrate);
        this.monthly_bonus = monthly_bonus;
        this.training_hours_completed = training_hours_completed;
    }

    public int getMonthly_bonus() {
        return monthly_bonus;
    }
    public int getTraing_hours_required() {
        return traing_hours_required;
    }
    public int getTraining_hours_completed() {
        return training_hours_completed;
    }



    public void setMonthly_bonus(int monthly_bonus) {
        this.monthly_bonus = monthly_bonus;
    }
    public void setTraining_hours_completed(int training_hours_completed) {
        this.training_hours_completed = training_hours_completed;
    }
}
