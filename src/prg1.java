//Chris Hinson Chapter 10
//program 1 demonstration class

import java.util.Scanner;

public class prg1 
{
    public static void main(String args[])
    {
        Scanner k = new Scanner(System.in);
        System.out.println("Employee name?");
        String name = k.next();
        System.out.println("Employee id? - please use correct numbernumbernumber-letter formatting");
        String id = k.next();
        System.out.println("Hire date?(mm/dd/yyyy)");
        String hire_date = k.next();
        System.out.println("Shift? (1=day, 2= night)");
        int shift = k.nextInt();
        System.out.println("Hourly pay rate?");
        double payrate = k.nextDouble();
        
        ProductionWorker chris = new ProductionWorker(name,id,hire_date,shift,payrate);
    }
}
