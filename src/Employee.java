//Chris Hinson Chapter 10
//Employee Superclass

public class Employee
{
    private String name;
    private String id;
    private String hire_date;

    public Employee(String name, String id, String hire_date)
    {
        this.name=name;
        this.id = id;
        this.hire_date = hire_date;
    }

    public String getName() {
        return name;
    }
    public String getId() {
        return id;
    }
    public String getHire_date() {
        return hire_date;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setHire_date(String hire_date) {
        this.hire_date = hire_date;
    }
}
