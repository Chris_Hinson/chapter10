//Chris Hinson Chapter 10
//Program 1 subclass

public class ProductionWorker extends Employee
{
    protected int shift;
    protected double payrate;

    public ProductionWorker(String name, String id, String hire_date, int shift, double payrate) {
        super(name, id, hire_date);
        this.shift = shift;
        this.payrate = payrate;
    }

    public int getShift() {
        return shift;
    }
    public double getPayrate() {
        return payrate;
    }

    public void setShift(int shift) {
        this.shift = shift;
    }
    public void setPayrate(double payrate) {
        this.payrate = payrate;
    }
}
